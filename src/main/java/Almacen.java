/**
 * Pilar Pi�eiro. Ejercicio2
 */
public interface Almacen {
  boolean eliminarArticulo(String articulo, int cantidad) ;
  boolean hayArticulos(String articulo, int cantidad) ;
}
