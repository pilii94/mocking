import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;

public class OrdenDeCompraTest {

	//si al efectuar una compra hay productos 
	//en el almac�n los dos m�todos de �ste se han invocado
	
		 @Test
		 public void IfTheresEnoughProductsTwoMethodesAreInvokedTest() {
		 // Create mock
		 Almacen almacen= mock(Almacen.class);
		 //Stubbing
		 when (almacen.hayArticulos("Cerveza",20)).thenReturn(true) ;
		 // Executing
		 OrdenDeCompra orden = new OrdenDeCompra("Cerveza", 20) ;
		 orden.efectuarCompra(almacen) ;

		 // Verify
		 verify(almacen).eliminarArticulo("Cerveza", 20);
		 verify(almacen).hayArticulos("Cerveza", 20);
		 }
		
	
	//si no hay suficientes productos compruebe que 
	//�stos no se han elminado del almac�n
		 @Test
		public void IfTheresNotEnoughProductsNoneAreDeletedTest() {
		// Create mock
		Almacen almacen= mock(Almacen.class);
		//Stubbing
		when (almacen.hayArticulos("Cereal",10)).thenReturn(false) ;
		// Executing
		OrdenDeCompra orden = new OrdenDeCompra("Cereal", 10) ;
		orden.efectuarCompra(almacen) ;
		
		// Verify
		verify(almacen,never()).eliminarArticulo("Cereal", 10);
		 }
		}
	

