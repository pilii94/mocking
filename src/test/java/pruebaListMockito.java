import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Modified by Pilar Pińeiro 10/03/2016
 */
public class pruebaListMockito {
	
  @Test(expected=RuntimeException.class)//Ejercicio1a
  public void pruebaLista() {
    // Creating the mock object
    List<String> list = mock(List.class) ;

    // Stubbing: defining the behavior
    when(list.get(0)).thenReturn("First") ;
    when(list.get(1)).thenReturn("Second") ;
    when(list.get(3)).thenReturn("Third") ;//Ejercicio1d
    when(list.get(2)).thenThrow(new RuntimeException()) ;

    // Using the mock object
    System.out.println(list.get(0));
    System.out.println(list.get(1));
    System.out.println(list.get(2));
    System.out.println(list.get(3));
    // Verifying
    verify(list, never()).get(2) ;//Ejercicio1b
    verify(list, atLeast(1)).get(3) ;//Ejercicio1c
    verify(list, times(1)).get(0) ;
    verify(list, times(1)).get(1);
  }
}